# Introduction to the Jiggle User Guide

Jiggle is a Graphical User Interface (GUI) software application used to analyze
earthquake waveform data and calculate accurate earthquake (event) parameters.

Jiggle is part of the post-processing (PP) software suite in the ANSS Quake
Monitoring System (AQMS).  ANSS = Advanced National Seismic System.

Human analysts at different Regional Seismic Networks (RSN) use Jiggle to come
up with a high-quality earthquake catalog. These catalogs are essential for
monitoring earthquake hazards in the region and form the basis for research
efforts in earthquake science.

Immediately following an earthquake, real-time (RT) software in AQMS will
automatically determine arrival times of seismic waves at different stations
across the region and solve for its approximate origin and magnitude.  An
earthquake's origin is defined as its location (latitude, longitude, depth) and
origin time (`YYYY-MM-DD HH:MM:SS.SSS` time when the earthquake started, in
Coordinated Universal Time [UTC]). However, the RT software may make mistakes in
identifying earthquakes, and the resulting earthquake parameters are not
accurate enough.  Therefore, analysts use the Jiggle GUI to visually review
event waveforms (time series of ground motion) at seismic stations, manually
pick the precise arrival time of P and S seismic waves, and update the event's
origin and magnitude.

This user manual has some content (configurations, databases, analyst practices)
that is specific to the setup at the [Southern California Seismic Network
(SCSN)](http://www.scsn.org), located at Caltech in Pasadena, California.

However, Jiggle is also used at other RSNs, such as: AVO, CERI, HVO, NCSN, NEIC,
MBMG, PNSN, UUSS, LCSN.

For the old Jiggle website, visit
[https://pasadena.wr.usgs.gov/jiggle/index.html](https://pasadena.wr.usgs.gov/jiggle/index.html).

# Roadmap

TODO
