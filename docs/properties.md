# Overview of Jiggle Properties

Jiggle requires several hundred input parameters, spread across multiple
properties files.

# List of Jiggle Properties Files

Sub-bullet points indicate filenames for separate properties files that are
specified in the parent bullet properties file.

* `jiggle_archdb2_waveserver.props`: main Jiggle user properties file with >500
input parameters
([https://pasadena.wr.usgs.gov/jiggle/JiggleProperties.html](https://pasadena.wr.usgs.gov/jiggle/JiggleProperties.html))
This main properties file includes channel time model parameters
([https://pasadena.wr.usgs.gov/jiggle/channeltimemodel.html](https://pasadena.wr.usgs.gov/jiggle/channeltimemodel.html))
    * `eventNew.props`: properties defining the event catalog selection filter
    settings
    ([https://pasadena.wr.usgs.gov/jiggle/eventselection.html](https://pasadena.wr.usgs.gov/jiggle/eventselection.html))
    * `openmapNew.props`: properties defining map setup and layers that can be
    added, removed, or modified
    ([https://pasadena.wr.usgs.gov/jiggle/openmap.html](https://pasadena.wr.usgs.gov/jiggle/openmap.html))
        * `omap-views.properties`: properties defining map views near specific
	regions
        * `omap-netlayers.properties`: properties defining map layer display
	settings
	([https://pasadena.wr.usgs.gov/jiggle/maplayerprops.html](https://pasadena.wr.usgs.gov/jiggle/maplayerprops.html))
        * `omap-layernames.properties`: properties defining names of map layers
        * `omap-bbnlayers.properties`: properties defining map layer settings
    * `mdMagEng.props`: properties defining magnitude engine setup for
    M<sub>d</sub> (duration magnitude)
    ([https://pasadena.wr.usgs.gov/jiggle/magprops.html](https://pasadena.wr.usgs.gov/jiggle/magprops.html))
    * `mlMagEng.props`: properties defining magnitude engine setup for
    M<sub>L</sub> (local magnitude)
    ([https://pasadena.wr.usgs.gov/jiggle/magprops.html](https://pasadena.wr.usgs.gov/jiggle/magprops.html))
    * `mlMagMeth2.props`: properties defining magnitude method setup for
    M<sub>L</sub> (local magnitude)
    ([https://pasadena.wr.usgs.gov/jiggle/magprops.html](https://pasadena.wr.usgs.gov/jiggle/magprops.html))
    * `hypoMdMagMeth.props`: properties defining magnitude method setup for
    M<sub>d</sub> (duration magnitude)
    ([https://pasadena.wr.usgs.gov/jiggle/magprops.html](https://pasadena.wr.usgs.gov/jiggle/magprops.html))
    * `pickEW.props`: properties for automatic phase picker
    ([https://pasadena.wr.usgs.gov/jiggle/picker.html](https://pasadena.wr.usgs.gov/jiggle/picker.html))
        * `pickEW-alt3.parms`: properties for automatic phase picker at every
	channel

The Jiggle website
[https://pasadena.wr.usgs.gov/jiggle/Install.html](https://pasadena.wr.usgs.gov/jiggle/Install.html)
has examples of properties files in `properties.zip`. However, I recommend
getting a copy of existing properties files from an experienced seismic analyst
at your RSN, and then modifying them for your specific needs.

# View Properties within Jiggle

![Screenshot](img/jiggle_view_properties.png)

Jiggle user properties can be viewed within the Jiggle GUI (see screenshot
above), by selecting either of these two options from the top menu bar:

*  <b>Properties</b> &rarr; <b>Property info</b>
*  <b>Dump</b> &rarr; <b>Property info</b>

# Modify Properties within Jiggle

To modify the properties, users can do either of the following:

* Directly edit the text files containing the Jiggle properties, then load them
into Jiggle with "<b>Properties</b> &rarr; <b>Load properties...</b>"
from the top menu bar
* Edit the properties within the GUI Preferences using "<b>Properties</b>
&rarr; <b>Edit properties...</b>" from the top menu bar (see screenshot
below)

![Screenshot](img/properties_preferences.png)

# Database Connection Properties

Jiggle was intially only able to connect to Oracle databases, and will use
default values for some database parameters if they are not explicitly specified
in the properties file. For Jiggle to work correctly with PostgreSQL it
is important to explicitely specify the jasiObjectName (PNSN), the dbaseDriver
(org.postgreSQL.Driver), dbaseSubprotocol (postgresql), and the dbasePort.
See Table 1 below for the parameters that need to be set in your Jiggle
properties file to successfully connect to an Oracle or PostgreSQL AQMS database.

**Table 1**: Jiggle database connection properties

| Parameter | Description | AQMS Oracle | AQMS PostgreSQL |
|-----------|-------------|--------|------------|
| jasiObjectType | schema type | TRINET (optional, default=TRINET) | **required: *PNSN*** |
| useLoginDialog | show dialog box for user to log in when set to true | optional (default=false) | optional (default=false)
| dbaseHost | hostname | **required** | **required** |
| dbaseDomain | domain name, e.g. ess.washington.edu | optional | optional |
| dbaseName | database name, e.g. archdb | **required** | **required** |
| dbasePort | database server port number |optional (default=1521)| **required** |
| dbaseDriver | name of JDBC database driver | **required: *oracle.jdbc.driver.OracleDriver*** | **required: *org.postgreSQL.Driver*** |
| dbaseUser  | database user with read/write/execute privileges for full functionality | **required** | **required** |
| dbasePasswd | database password of database user |  **required** | **required** |
| dbaseSubprotocol  | type of JDBC subprotocol | optional (default is *oracle:thin*, other option is *oracle:oci* if the server where you run Jiggle has Oracle libraries on it) | **required: *postgresql*** |
| dbaseTNSName | usually the same as dbaseName, should match TNSNames.ora entry when using *oracle:oci* subprotocol | optional (required when using *oracle:oci*) | optional |
| dbLastLoginURL | is updated automatically | optional | optional |
| dbAttributionEnabled | Jiggle user name saved in database when set to true | optional (default=true)| optional (default=true) |
| dbWriteBackEnabled | Jiggle will write and commit changes to the database when set to true, read-only when not | optional (default=true) | optional (default=true) |
| debugSQL | verbose SQL output to screen or logfile when set to true| optional (default=false) | optional (default=false) |
| debugCommit | verbose messages regarding commit status when set to true | optional (default=false) | optional (default=false) |


It is also possible to set (or reset) the database connection parameters via
the "<b>Properties</b> &rarr; <b>Edit properties</b> &rarr; <b>Event
DataSource</b>" from the top menu bar (see screenshot below). For PostgreSQL
systems it is important to specify all required settings. dbWriteBackEnabled and dbAttributionEnabled can be edited via the <b>DB Save</b> menu item.

![Screenshot](img/properties_event_datasource.png)
