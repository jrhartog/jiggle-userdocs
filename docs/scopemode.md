# Scope Mode Introduction

TODO

View continuous data at all channels, up to 10 minutes

Use for station quality control

Run within Jiggle – snapshots

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## Example Image

Cupcake indexer is a snazzy new project for indexing small cakes.

![Screenshot](img/jiggle_view_properties.png)

*Above: Cupcake indexer in progress*

