# Overview

Jiggle install instructions are on this website:
[https://pasadena.wr.usgs.gov/jiggle/Install.html](https://pasadena.wr.usgs.gov/jiggle/Install.html)
(TODO: clean up the install instructions)

Jiggle only interfaces with the [California Integrated Seismic Network (CISN)
database schema](https://www.ncedc.org/db/Documents/NewSchemas/schema.html). Use
by other schema would require creation of a concrete implementation of [Java
Abstract Seismic Interface
(JASI)](https://pasadena.wr.usgs.gov/jiggle/codehelp.html).  Jiggle is intended
only for use by RSNs, not by the general public.

Jiggle runs on a Java Virtual Machine (JVM), so it is platform independent.  The
user should be able to run Jiggle on a Windows, Mac, or Linux operating system.
JVM version 1.8 or higher is recommended.

Jiggle connects to an Oracle database through the Java Database Connectivity
(JDBC) application programming interface (API).

Jiggle may run slowly if accessed through a remote connection. Users have
reported faster performance over Remote Desktop, than through a Virtual Private
Network (VPN).

# Requirements to run Jiggle

* A reasonably fast processor and at least 2 GB memory
* Java Platform, Standard Edition (Java SE): 8 or 11.  If it is not already
installed on your computer, download it at
[https://www.oracle.com/technetwork/java/javase/downloads/index.html](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Application and external .jar files](#external-libraries-and-dependencies)
* [Jiggle properties files with input parameters](../properties)
* [Script to start Jiggle GUI](#jiggle-runtime-script)
* Oracle database with [CISN
schema](https://www.ncedc.org/db/Documents/NewSchemas/schema.html), JASI
views, and PL/SQL commands

# Jiggle versions

Create a directory for the Jiggle jar files and other dependencies.  For
example, let's call this directory `bin/`.

The latest version of Jiggle is Java8 `jiggle18-p7.jar` from 2017-06-27 (see
most recent entry in the [Jiggle Release
Notes](https://pasadena.wr.usgs.gov/jiggle/currentRelNotes.html).

Download this jiggle jar file from
[https://pasadena.wr.usgs.gov/jiggle/Install.html](https://pasadena.wr.usgs.gov/jiggle/Install.html),
rename it `jiggle.jar`, and put it in the `bin/` directory.  As new features get
added to Jiggle, this jar file will be updated on the website, for the user to
download the latest version.

This version of Jiggle from 2017-06-27 depends on having the up-to-date GTYPE
schema in the database. Most RSNs are now using the GTYPE schema, although there
are still a few exceptions in early 2019. 

* In the origin database table, there is a separate column for GTYPE.
* If an event type is "earthquake", the origin can have a GTYPE value of either
"local", "regional", or "teleseism".

# External libraries and dependencies

All external libraries and dependencies for Jiggle (`.jar`, `.zip` files)
mentioned below are available for download from
[https://pasadena.wr.usgs.gov/jiggle/Install.html](https://pasadena.wr.usgs.gov/jiggle/Install.html).

Jiggle depends on the following external .jar files which should also go into
the `bin/` directory. Do not unpack these jar files.  New users need to download
these only once.

* `ojbdc8.jar` (formerly `ojbdc7.jar`)
* `acme.jar`
* `openmap.jar`
* `QWClient.jar`
* `swarm.jar`
* `seed-pdcc.jar`
* `looks-2.0.4.jar`
* `forms-1.0.7.jar`
* `colt.jar`
* `usgs.jar`
* `jregex1.2_01.jar`

Jiggle needs supporting map interface files. Download `mapdata.zip` which should
unzip and be placed into a `mapdata/` subdirectory inside the `bin/` directory.
New users need to download these only once.

Jiggle also needs properties files, which contain input parameter names and
values. Download `properties.zip` for examples. See [Properties (Input
Parameters)](../properties) for more information about these properties files.

# Jiggle runtime script

To run Jiggle, the user needs a runtime script (Linux/Unix/Mac) or bat file
(Windows).  This script should be in the same `bin/` directory as the
`jiggle.jar` file.

For example, a Unix runtime script, named `run_jiggle.sh`, would look like:

    #!/bin/bash

    java -Xms1000M -Xmx2560M -DJIGGLE_HOME=./ -DJIGGLE_USER_HOMEDIR=$HOME/Documents/Jiggle/Code/prop -jar jiggle.jar $1

Explanation of input arguments:

* `-Xms`: initial and minimum Java heap size (in this case, 1000 MB) to allocate
memory for the JVM
* `-Xmx`: maximum Java heap size (in this case, 2560 MB) to allocate memory for
the JVM
* `-DJIGGLE_HOME`: directory containing the Jiggle application’s files such as
`jiggle.jar`
    * In this case, `-DJIGGLE_HOME` is the current directory, assuming that this
    runtime script also lives in the `bin/` directory)
* `-DJIGGLE_USER_HOMEDIR`: directory containing the Jiggle properties files. See
[Properties (Input Parameters)](../properties) for more information.
    * This should be an absolute path; I have found that specifying a relative
    path does not work.
* The last `$1` in this command is there to specify a different Jiggle
properties filename as an input argument. Without the `$1` input, Jiggle assumes
the input parameters are in a file called `jiggle.props` within the
`-DJIGGLE_USER_HOMEDIR` directory.

Note that we need to make `run_jiggle.sh` executable:

    $ chmod +x run_jiggle.sh

To run Jiggle from the command line, with input parameters from file
`jiggle.props` located in directory `-DJIGGLE_USER_HOMEDIR`:

    $ ./run_jiggle.sh

To run Jiggle from the command line, with input parameters from a different file
`jiggle_waves.props` located in a different directory `../prop/`: 

    $ ./run_jiggle.sh ../prop/jiggle_waves.props




